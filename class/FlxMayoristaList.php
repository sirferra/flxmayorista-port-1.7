<?php
Class FlxMayoristaList extends ObjectModel
{
     /** @var integer id shop */
    public $idShop;

    /** @var integer id Lang */
    public $idLang;

    public $php_self = 'product';

    /** @var Product */
    public $product;

    /** @var Category */
    public $category;

    /** @var html el html de respuesta ;D */
    public $html;
    
    /**
     * @see ObjectModel::$definition
     */

    // public function __construct($idShop, $idLang, $categoryID){
    //     $category = new Category($categoryID);
    // }
    public function __construct(){
        
        $html = '<div><p style="font-size:30px"> Hola que tal! soy un módulo >:)</p></div>';
    }

    public function initContent()
	{
		//parent::initContent();
		//return  '<div><p style="font-size:30px"> Hola que tal! soy un controller >:)</p></div>';
		$this->setTemplate($this->module->getLocalPath().'/views/front/product-list-mayorista.tpl');
	}

    public function add($autodate = true, $null_values = false)
    {
        if (!parent::add($autodate, $null_values))
            return false;
    }

    public function update($null_values = false)
    {
        $return = parent::update($null_values);
        return $return;
    }

    public function delete()
    {
        $result = parent::delete();
        if (!$result)
            return false;
        return true;
    }

    
}
?>