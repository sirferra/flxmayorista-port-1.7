<?php
/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* // $response = array("Hola"=>"Como estas? :D");
* // die(json_encode($response));
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Éste módulo fue creado para mostrar datos para una tienda mayorista.
* Si notás algún harcodeo, me libero de toda responsabilidad (?). 
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    Flexxus SA <leandro.ferrarotti@flexxus.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class FlxMayorista extends Module{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'flxmayorista';
        $this->tab = 'front_office_features';
        $this->version = '3.0.2';
        $this->author = 'Flexxus S.A.';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         * dice 1.6, no se. 
         */
        // $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Flexxus Mayorista');
        $this->description = $this->l('Modifica la segunda vista del listado de productos');

        $this->confirmUninstall = $this->l('¿Estás seguro de eliminar la vista mayorista?');

        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
    }

    public function install()
    {
        $this->registerHook('displayBeforeBodyClosingTag');
        $this->registerHook('displayHeader');
        return parent::install() &&
            $this->rewriteProductList('install') &&
            $this->registerHook('displayFooter') &&
            Configuration::updateValue('FLX_SHOWVIEW', 'grid') &&
            Configuration::updateValue('FLX_STOCKMAX', 100) &&
            Configuration::updateValue('FLX_STOCKMED', 50) &&
            Configuration::updateValue('FLX_STOCKMIN', 20) &&
            Configuration::updateValue('FLX_SHOWSTOCK', 1) &&
            Configuration::updateValue('FLX_SHOWMARGIN', 0);
            Configuration::updateValue('FLX_ORDERONEPAGE', 1);
    }

    public function uninstall()
    {
        if( 
            $this->rewriteProductList('uninstall') &&
            !parent::uninstall()           
         )
            return false;
        return true;
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitFlxmayoristaModule')) == true) {
            $this->postProcess();
        }

        $this->context->smarty->assign('module_dir', $this->_path);

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output.$this->renderForm();
    }
    public function getConfigFieldsValues()
	{
		return array(
            'FLX_SHOWVIEW'  => Configuration::get('FLX_SHOWVIEW'),
			'FLX_STOCKMAX'  => Configuration::get('FLX_STOCKMAX'),
            'FLX_STOCKMED'  => Configuration::get('FLX_STOCKMED'),
            'FLX_STOCKMIN'  => Configuration::get('FLX_STOCKMIN'),
            'FLX_SHOWSTOCK' => Configuration::get('FLX_SHOWSTOCK'),
            'FLX_SHOWMARGIN' => Configuration::get('FLX_SHOWMARGIN'),
            'FLX_ORDERONEPAGE' => Configuration::get('FLX_ORDERONEPAGE'),
		);
    }
    /**
     * Renderiza el form en el back.
     * Esto es lo que muestra cuando le das a configurar al módulo.
     * Es una bosta para editarlo, pero si, es acá. 
    */
    public function renderForm()
	{
        $selectViews[0] = array('value' => 'grid','name' => 'Vista Cuadricula');
        $selectViews[1] = array('value' => 'list','name' => 'Vista Listado');
        $selectViews[2] = array('value' => 'mayorista','name' => 'Vista Mayorista');
		
        $fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Settings'),
					'icon' => 'icon-cogs'
                ),
                    'input' => array(
                        array(
                            'type' => 'select',
                            'label' => $this->l('Tipo de Vista'),
                            'name' => 'FLX_SHOWVIEW',
                            'required' => true,
                            'options' => array(
                                'query' => $selectViews,
                                'id' => 'value',                      
                                'name' => 'name' 
                            )
                    ),    
					array(
						'type' => 'text',
						'label' => $this->l('Maximum Stock'),
						'name' => 'FLX_STOCKMAX',
						'desc' => $this->l('Set the maximum stock to show green color.'),
					),
                    array(
						'type' => 'text',
						'label' => $this->l('Medium Stock'),
						'name' => 'FLX_STOCKMED',
						'desc' => $this->l('Set the Medium stock to show yellow color.'),
					),
                    array(
						'type' => 'text',
						'label' => $this->l('Minimum Stock'),
						'name' => 'FLX_STOCKMIN',
						'desc' => $this->l('Set the Minimum stock to show red color.'),
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Show Stock'),
						'name' => 'FLX_SHOWSTOCK',
						'desc' => $this->l('It must be set to show the available stock.'),
						'values' => array(
									array(
										'id' => 'active_on',
										'value' => 1,
										'label' => $this->l('Mostrar')
									),
									array(
										'id' => 'active_off',
										'value' => 0,
										'label' => $this->l('Ocultar')
									)
								),
					),
                    array(
						'type' => 'switch',
						'label' => $this->l('Show Margin'),
						'name' => 'FLX_SHOWMARGIN',
						'desc' => $this->l('It must be set to show the available Margin.'),
						'values' => array(
									array(
										'id' => 'active_on',
										'value' => 1,
										'label' => $this->l('Si')
									),
									array(
										'id' => 'active_off',
										'value' => 0,
										'label' => $this->l('No')
									)
								),
					),
                    array(
						'type' => 'switch',
						'label' => $this->l('Order One Page'),
						'name' => 'FLX_ORDERONEPAGE',
						'desc' => $this->l('Show order in one step.'),
						'values' => array(
									array(
										'id' => 'active_on',
										'value' => 1,
										'label' => $this->l('Si')
									),
									array(
										'id' => 'active_off',
										'value' => 0,
										'label' => $this->l('No')
									)
								),
					),
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);

		$helper = new HelperForm();
		$helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitFlxmayoristaModule';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);

		return $helper->generateForm(array($fields_form));
    }

    /**
     * Las configs. Acá se setean. 
     * Te preguntaste alguna vez donde van a parar los valores que seteas en el back?
     * bueno, aquí está la respuesta, aquí está la verdad ♪
     */
    protected function postProcess()
    {
        Configuration::updateValue('FLX_SHOWVIEW', Tools::getValue('FLX_SHOWVIEW'));
        Configuration::updateValue('FLX_STOCKMAX', Tools::getValue('FLX_STOCKMAX'));
        Configuration::updateValue('FLX_STOCKMED', Tools::getValue('FLX_STOCKMED'));
        Configuration::updateValue('FLX_STOCKMIN', Tools::getValue('FLX_STOCKMIN'));
        Configuration::updateValue('FLX_SHOWSTOCK', Tools::getValue('FLX_SHOWSTOCK'));
        Configuration::updateValue('FLX_SHOWMARGIN', Tools::getValue('FLX_SHOWMARGIN'));
        Configuration::updateValue('FLX_ORDERONEPAGE', Tools::getValue('FLX_ORDERONEPAGE'));
        return true;
    }

    //En desuso, por ahora.   
    public function displayAjax(){
        return $this->fetch('module:flxmayorista/views/templates/front/list.tpl');
    }

    public function hookHeader($params){
        $this->hookFooter($params);
        $this->hookDisplayBeforeBodyClosingTag($params);
        return null;
    }

    /**
     * Hookea el estilo, el css desde un tpl. 
     * No es lo mas amigable, pero weno. 
     */
    public function hookFooter($params)
    {
       return $this->fetch('module:flxmayorista/views/templates/hook/footerStyle.tpl');
    }
    
    /**
     * Esta función, es practicamente la que muestra el mayorista
     * Si necesitás pasarle parámetros a la vista, podés hacerlo desde acá.
     * Incluso me aventuro a decir que no sería necesario rewritear el product-list si lo manejás bien
     * desde acá, pero eso es otro futuro caótico en el que no me voy a ver envuelto por ahora(?)
     */
    public function hookDisplayBeforeBodyClosingTag($params) {
        $this->smarty->assign('stockLimits', array(
            'max' => Configuration::get('FLX_STOCKMAX'),
            'med' => Configuration::get('FLX_STOCKMED'),
            'min' => Configuration::get('FLX_STOCKMIN'),
            'showStock' => Configuration::get('FLX_SHOWSTOCK')
        ));
        return $this->fetch('module:flxmayorista/views/templates/hook/script.tpl');
    }

    /**
     * Este método reemplaza el product-list dentro del warehouse. Lo hace solo al instalar y desinstalar el módulo.
     * La idea, en un futuro, es hacer otros casos para el switch donde detecte otras estructuras de themes
     * la actual es: themes/warehouse/templates/catalog/_partials/miniatures/product-list.tpl
     */
    public function rewriteProductList($action){
        $themeName = '';
        /*$themeName = Db::getInstance()->getValue('SELECT t.directory
                                                  FROM '._DB_PREFIX_.'shop s
                                                  INNER JOIN '._DB_PREFIX_.'theme t ON t.id_theme = s.id_theme
                                                WHERE s.id_shop = '.Context::getContext()->shop->id);
        */
        switch ($action) {
            case 'install':
                switch($themeName){
                    default: //Le pongo default, porque es válido solo para warehouse por ahora. Acá pondrías tus otros themes.
                        $rename = rename(_PS_ROOT_DIR_.'/themes/warehouse/templates/catalog/_partials/miniatures/product-list.tpl',dirname(__FILE__).'/theme/tmp/product-list.tpl');
                        $copiar = copy(dirname(__FILE__).'/theme/rewrite/product-list.tpl',_PS_ROOT_DIR_.'/themes/warehouse/templates/catalog/_partials/miniatures/product-list.tpl');
                        if($rename&&$copiar){return true;}else{return false;}
                    break;
                }
                break;
            case 'uninstall':
                switch($themeName){
                    default:
                        $borrar = unlink(_PS_ROOT_DIR_.'/themes/warehouse/templates/catalog/_partials/miniatures/product-list.tpl');
                        $rename = rename(dirname(__FILE__).'/theme/tmp/product-list.tpl',_PS_ROOT_DIR_.'/themes/warehouse/templates/catalog/_partials/miniatures/product-list.tpl');
                        if($rename&&$borrar){return true;}else{return false;}
                    break;
                }
                break;
        }
    }
}



    
?>