<?php

// Edit name and class according to your files, keep camelcase for class name.
require_once _PS_MODULE_DIR_.'flxmayorista/flxmayorista.php';

class FlxMayoristaFrontController extends ModuleFrontController
{
    public function initContent()
    {

		$response = '<div id="hola">Soy el controller D:<</div>';
        // Classic json response
        $json = Tools::jsonEncode($response);
        echo $json;
        die;

        // For displaying like any other use this method to assign and display your template placed in modules/modulename/views/template/front/...
        // Just put some vars in your template
        // $this->context->smarty->assign(array('var1'=>'value1'));
        // $this->setTemplate('template.tpl');

        // For sending a template in ajax use this method
        // $this->context->smarty->fetch('template.tpl');

    }

}

?>