# Mayorista 1.7 (traído desde 1.6)

Hola ciudadano promedio. Este es el mayorista de 1.7<br>
No, no está desarrollado. Es una adaptación. Por ende, aún <br>
funciona con la lógica que usaba 1.6:

- La vista se carga por javascript:
  - eso quiere decir que la construye LUEGO de que carga la página. en el navegador
 no en el server. Basura. Completa y total escoria.
  - Los eventos son por javascript. "eventos". USo los de presta. Masomenos
- Actualmente, no copia bien los hooks y eso
- Ya dije que esto es basura? Bueno. lo es. 

# [Lo que te interesa está acá]
- Para subir un módulo a Presta tenes que comprimirlo en zip. 
- No voy a insultar tu inteligencia diciéndote como comprimir en zip, ni como diferenciar un zip de un rar. En caso de que no lo sepas, google es tu amiguis. 
- Descargá lo de release. Ahí están las versiones "estables" (jaja estable dice este borracho)
- El zip, contiene una carpeta así:

```javascript
.
└─ flxmayorista-port-1.7-v[version]  Esto es lo que tenes que renombrar a "flxmayorista"
    ├── class [Useless]
    │     └─ FLxMAyoristaList.php
    ├── controller [Useless]
    │     └─ front
    │         ├─ ajax.php
    │         ├─ ajax.php_
    │         └─ mayoristaFrontController.php
    ├── theme 
    │     ├─ rewrite (acá está el tpl que sobreescribe el del tema)
    │     │   └─ product-list.tpl (themes/warehouse/templates/catalog/_partials/miniatures)
    │     └─ tmp
    │         └─ product-list.tpl (el original, lo guarda acá por las dudas.)
    ├── views
    │     └─ rewrite
    │         ├─ admin
    │         │    └─ configure.tpl   (el configure del adminmto)
    │         └─ hook
    │              ├─ footerStyle.tpl (acá están los estilos de css)
    │              └─ script.tpl      (acá está el script principal)
    ├── README.md
    ├── config.xml
    ├── config_es.xml
    ├── flxmayorista.php              (Lo que instala el módulo)
    ├── logo.gif
    ├── logo.png
```
