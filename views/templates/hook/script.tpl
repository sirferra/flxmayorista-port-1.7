{literal}
<script>
/* Mayorista view */
/* 
	TODO: consulta ajax para determinar el stock que queda de un producto
	Feature: paginado
	TODO: configurable vars in backoffice
	Feature: animate transition between views;
	FixedBug: Al cliclear la vista grid, y volver al mayorista, no mantiene las columnas.
*/

$(document).ajaxSend((s,i,r)=>{
	if(typeof r.url  !== 'undefined' && (r.url.includes('page') || r.url.includes('from-xhr'))){bindGrid();}

});
$(document).ajaxComplete((s,i,r)=>{
	if(typeof r.url  !== 'undefined' && (r.url.includes('page') || r.url.includes('from-xhr'))){bindGrid();}
	selectLi(localStorage.getItem('RealView'));
});
$(document).ready(()=>{bindGrid();});

function bindGrid(){
	
	var view = localStorage.getItem('display');
	if(view == "null"){
		localStorage.setItem('display', 'list');
	};
	/*Wth is this, Leandro from the past?*/
	if(realView != view){
		view = localStorage.getItem('RealView');
	}
	var realView = localStorage.getItem('RealView', view);
	removeMayorista();
	if(view == 'grid'){
		display(view);
	}else{
		transformToMayorista();
	}
	
	$(document).on('click', '#grid', function(e){
		e.preventDefault();
		animateDisplay('grid');
	});

	$(document).on('click', '#list', function(e){
		e.preventDefault();
		console.log('%cPoderes de mayorista, ACTIVENSE!', 'background-color: #333; color: #4cd137');
		animateDisplay('list');
	});

    //addMayoristaListButton();
	/*
	$('#js-product-list-top').each((i,o)=>{
		$(o).find('li').click(()=>{
			removeMayorista();
		});
	});
	$('li#mayorista').click((e)=>{
		e.preventDefault();
		var view = localStorage.getItem('display','mayorista');
		localStorage.setItem('RealView', 'mayorista');
		transformToMayorista();
	});*/
	//selectLi(view);
}

function addMayoristaListButton(){
    if(typeof $('#js-product-list-top').find('ul')[0] !== 'undefined'){
		if($('#js-product-list-top').find('ul').find('#mayorista').length ==0){
        	$('#js-product-list-top').find('ul').append('<li id="mayorista"><a href="#" title="Mayorista">'
			+'<i class="icon-th icon-Mayorista fa-list">'
			+'</a></li>');
		}
	}else if(typeof $('div.view-switcher') != 'undefined'){
		 $('div.view-switcher').append('<a href="#" class="js-search-link" title="Mayorista">'
            +'<i class="icon-th icon-Mayorista fa-list">'
            +'</a>')
    }else{
        return false;        
    }
    
}

function animateDisplay(view){
	var list = $('#js-product-list');
		list.css('opacity',1);
		list.animate({
			opacity: 0
		},400,'easeInExpo',()=>{
			list.animate({
				opacity:1
			},400,'easeInExpo');
			display(view);
		});
		localStorage.setItem('RealView', view);
	selectLi(view);
}

function display(view){
	/*
	if (view == 'list'){
		$('#products div.products').removeClass('grid').addClass('list');
		$('#products div.products > article').removeClass('col-xs-12 col-sm-6 col-md-4 col-lg-6 col-xl-4').addClass('col-xs-12');
		$('#products div.products > article').each(function(index, element) {
			var html = '';
			html = '<div class="product-container">';
				html += '<div class="row">';
					html += '<div class="thumbnail-container col-xs-12 col-sm-6 col-md-4 col-lg-4">' + $(element).find('.thumbnail-container').html() + '</div>';
					html += '<div class="product-description col-xs-12 col-sm-6 col-md-8 col-lg-8">';
						html += '<h1 class="h3 product-title" itemprop="name">'+ $(element).find('.product-title').html() + '</h1>';
						var price = $(element).find('.product-price-and-shipping').html();
						if (price != null) {
							html += '<div class="product-price-and-shipping">'+ price + '</div>';
						}
						html += '<p class="product-desc" itemprop="description">'+ $(element).find('.product-desc').html() + '</p>';
						var colorList = $(element).find('.highlighted-informations').html();
						if (colorList != null) {
							html += '<div class="highlighted-informations hidden-sm-down">'+ colorList +'</div>';
						}
						html += '<div class="button-container">'+ $(element).find('.button-container').html() +'</div>';
					html += '</div>';
				html += '</div>';
			html += '</div>';
			$(element).html(html);
		});
		$('.display').find('li#list').addClass('selected');
		$('.display').find('li#grid').removeAttr('class');
		$('.display').find('li#mayorista').removeAttr('class');
		localStorage.setItem('display', 'list');
	}else{ */
	if(view='grid'){
		$('#products div.products').removeClass('list').addClass('grid');
		$('#products div.products > article').removeClass('col-xs-12').addClass('col-xs-12 col-sm-6 col-md-4 col-lg-6 col-xl-4');
		$('#products div.products > article').each(function(index, element) {
			var html = '';
			html += '<div class="product-container">';
				html += '<div class="thumbnail-container">' + $(element).find('.thumbnail-container').html() + '</div>';
				html += '<div class="product-description">';
					html += '<h1 class="h3 product-title" itemprop="name">'+ $(element).find('.product-title').html() + '</h1>';
					var price = $(element).find('.product-price-and-shipping').html();
					if (price != null) {
						html += '<div class="product-price-and-shipping">'+ price + '</div>';
					}
					html += '<p class="product-desc" itemprop="description">'+ $(element).find('.product-desc').html() + '</p>';
					var colorList = $(element).find('.highlighted-informations').html();
					if (colorList != null) {
						html += '<div class="highlighted-informations hidden-sm-down">'+ colorList +'</div>';
					}
				html += '</div>';
			html += '</div>';
			$(element).html(html);
		});
		$('.display').find('li#grid').addClass('selected');
		$('.display').find('li#list').removeAttr('class');
		$('.display').find('li#mayorista').removeAttr('class');
		localStorage.setItem('display', 'grid');
	}
}
function addMayorista(o){
	// Architecture
	// Article col-sm-12 col-lg-12
	//   .product-container
	//      .thumbnail-container  col-lg-2
	//		.product-description  col-lg-6
	//      AÑADIR AQUI LA PARTE MAYORISTA col-lg-4
	let thumb = $(o).find('.thumbnail-container');
	let proDes = $(o).find('.col-description');
	let colBuy = $(o).find('.col-buy');
	let input;
	let container = $(o).find('.product-miniature-list-row');

	var idProduct = $(o).data('id-product');
	var quantity = $(o).data('quantity');
	var price = $(o).data('price');
	if(typeof price == 'undefined'){
		return;
	}
	var price = price.replace(/\s*[A-Z]+/gmi,'');
	var price = price.replace(/(\$+\s)/gm,'');
	var idProductAttribute = $(o).data('id-product-attribute');
	var manufacturer = $(o).data('manufacturer');
	var reference = $(o).data('reference');
	var title = $(o).data('title');
	var formatedPriced = $(o).find('[itemprop=price]').text();
	var token = $(o).data('token');

	$(o).find('.button-container').hide();

	// Modificación de clases
	$(o).removeClass((i,c)=>{return (c.match(/col-[xs|md|lg]*-*[0-9]+/gm)|| []).join(' ');});
	thumb.removeClass((i,c)=>{return (c.match(/col-[xs|md|lg]*-*[0-9]+/gm)|| []).join(' ');});
	//proDes.removeClass((i,c)=>{return (c.match(/col-[xs|md|lg]*-*[0-9]+/gm)|| []).join(' ');});
	
	$(o).addClass('col-xm-12 col-lg-12');
	thumb.addClass('col-xm-6 col-lg-6'); // 6 es como el small
	//proDes.addClass('col-xm-6 col-lg-6');

	proDes.hide();
	colBuy.hide();
	
	// Duplicidad, para que no me agregue 30mil campos
	if($(o).find('#mayorista').length >0){
		$(o).find('#mayorista').remove();
	}
	if($(o).find('#description').length>0){
		$(o).find('#description').remove();
	}
	var htmlMayorista=
	'<div id="mayorista" class="bloque-Mayorista col-xm-4">'
		+'<div id="price_'+idProduct+'" class="formated-price">$ ' + price + ' <span class="sym-mult">x</div>'
		+'<div id="qty_'+ idProduct+'" class="qty_">'
			+'<div class="input-group bootstrap-touchspin" data-stock="'+quantity+'">'+'<span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>'
				+'<input type="text" name="qtyinput" id="quantity_wanted_'+idProduct+'" value="0" class="input-group form-control" style="display: block;width:60px;" '
					+ 'data-price="'+ price + '" data-id-product="'+idProduct+'">'
				+'<span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span>'
				+'<span class="input-group-btn-vertical">'
				+'</span>'
			+'</div>'
		+'</div>'
		+'<div id="total_'+idProduct+'" class="">$0.00</div>'
		+'<div id="tooltip_' +idProduct +'" class="tooltip-container"></div>';
	+'</div>'
	
	var htmlDescripcion =
	'<div id="description" class="col-xm-6 col-lg-6">'
		+'<div class="" itemprop="name">'
		+'<div class="blackbox">'
			+ '<span class="manufacturer">' + manufacturer + '</span> <i class="fa fa-tag"></i>   '
			+ '<span class="reference">'    + reference    + '</span><br>'
		+ '</div>'
		+ '<span class="title">'        + title		   + '</span><br>'
		+'</div>'
	+'</div>'
	
	$('#quantity_wanted_'+idProduct);
	container.append(htmlDescripcion);
	container.append(htmlMayorista);
	
	$('#quantity_wanted_'+idProduct).TouchSpin({
		//min: 0,
		//max: quantity,
		verticalbuttons: true,
	});

	quantityCss(idProduct, quantity);

	/* == Eventos para añadir el producto al carrito == */
	$('#quantity_wanted_'+idProduct).change((e)=>{
		// para que se quede dentro del input, porque si pulsas los botoncitos, se vuelve loco y empieza a sumar o restar artículos.
		$('#total_' + idProduct).text(
			calculateTotal(e.target.value, price)
		);
		$('#quantity_wanted_'+idProduct).focus();

	}).on('keydown',function(event){
		if(event.keyCode == 13){
			//Si pulso enter, que se dispare focusOut
			event.preventDefault();
			$(o).children('input').trigger('focusout');
			$(o).trigger('focusout');
		};
	}).focusout(function(event){
		// FocusOut, es quien agrega al carrito el producto
		// Como agregar cantidades: 
		// A los parámetros al actionURL, metele &qty= y la cantidad :D
		event.preventDefault();
		var valueToAdd = event.target.value;
		var _ret = (function () {
			var $form = $(event.target).closest('form');
			var query = "token=" + token + '&add=1&qty='+valueToAdd+'&action=update';

			var actionUrl = $form.attr('action');
			if(typeof actionUrl === 'undefined'){
				actionUrl="carrito?add=1&qty="+ valueToAdd +"&id_product="+ idProduct+ "&id_product_attribute="+ idProductAttribute;
			}else{
				if(!actionUrl.toString().includes(/(add)+|(id_product)+/gm)){
					actionUrl+="add=1&qty="+valueToAdd+"&id_product="+ idProduct+ "&id_product_attribute="+ idProductAttribute;
				}
			}	
				$.post(actionUrl, query, null, 'json').then(function (resp) {
					if(resp.success){
						var $BlockCart = $('#blockcart');
						var refreshURL = $BlockCart.data('refresh-url');
						if(typeof refreshURL == 'undefined'){
							refreshURL = $('.blockcart').data('refresh-url');
							$BlockCart = $('.blockcart');
						}	
						var requestData = {};
						if (event) {
							requestData = {
								id_product_attribute: idProductAttribute,
								id_product: idProduct
							};
						}
						//console.log(idProduct);
						$('#quantity_wanted_'+idProduct).val(0);
						$('#total_'+idProduct).text('$ 0.00');
						showToolTip('Agregado al Carrito!', idProduct);
						$.post(refreshURL, requestData).then(function (resp) {
							$BlockCart.replaceWith($(resp.preview));
							$('#mobile-cart-products-count').text($(resp.preview).find('.cart-products-count-btn').first().text());
							prestashop.emit('responsive updateAjax', {
								mobile: prestashop.responsive.mobile
							});
						}).fail(function (resp) {
							console.log("error 1");
							console.log(resp);
							prestashop.emit('handleError', {eventType: 'updateShoppingCart', resp: resp});
						});	
					}else{
						showToolTip("No hay suficiente stock!", idProduct);
					}
				}).fail(function (resp) {
					console.log({actionUrl,query})
					console.log("entro al fail");
					prestashop.emit('handleError', { eventType: 'addProductToCart', resp: resp });
				});
			})();
	});
	

}

function removeMayorista(){
	$('article').each((i,o)=>{
		$(o).find('#mayorista').remove();
		$(o).find('#description').remove();
		$(o).find('.button-container').show();
		$(o).find('.product-description').show();
	});
}

function transformToMayorista(){
	var list = $('#js-product-list');
	list.css('opacity',1);
	list.animate({
		opacity: 0
	},400,'easeInExpo',()=>{
		list.animate({
			opacity:1
		},400,'easeInExpo');
		list.find('.products:first').removeClass((i,c)=>{return (c.match(/col-[xs|md|lg]*-*[0-9]+/gm)|| []).join(' ');});;
		list.find('article').each((i,article)=>{
			addMayorista(article);
		});
	});
	

}

function quantityCss(id, qty){
	var bajo = 5 	// <rojo
	var alto = 25;	// >verde
					// El punto medio es un valor entre ambos
	$('#qty_'+id).removeClass((i,c)=>{return (c.match(/hight|low|medium/gmi)|| []).join(' ');});
	if(qty>=alto){
		$('#qty_'+id).addClass('hight');
	}else if(qty<alto && qty>=bajo){
		$('#qty_'+id).addClass('medium');
	}else if(qty<bajo){
		$('#qty_'+id).addClass('low');
	}
		
}


function calculateTotal(quantity, unit_price){
	unit_price = unit_price.replace(/[\.]+|[A-Z]+|\s/gm, '');
	unit_price = unit_price.replace(/,+/gm,'.');
	return `$ ${(parseFloat(unit_price) * quantity).toFixed(2)}`;
}
function showToolTip(msg,idProduct){
		html = '<span class="tooltip-text">'+ msg +'</span>';
		$('#tooltip_'+idProduct).append(html);
		$('#tooltip_'+idProduct).css('opacity',1);
		$('#tooltip_'+idProduct).animate({
			opacity: 0.25
		},2000,'easeInExpo',function(){
			$('#tooltip_'+idProduct).empty();
		});
}
function selectLi(view, display){
	$('.display').find('li#grid').removeAttr('class');
    $('.display').find('li#list').removeAttr('class');
    $('.display').find('li#mayorista').removeAttr('class');
    $('.display').find('li#' + view).addClass('selected');
    localStorage.setItem('display', view);

}
</script>
{/literal}