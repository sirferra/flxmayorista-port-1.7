<style>
:root{
    --staticGreen:#3D9700;
    --hoverOrange:#FF7F00;
    --staticRed: #D70000;
    --staticBlack: #333;
    --hoverGreen:#49b700;
    --hoverOrange: #FF8906;
    --hoverRed: #FF0000;
    --hoverBlack: #222;
}
.qty_{
    display: -webkit-inline-box;
    position: relative;
    margin-left: auto;
    margin-right: auto;
    margin-top: 20px;
    margin-bottom: 10px;
    height: 53px!important;
    max-height:53px!important;
    /* width: 30% */
}
div[id^="total_"]{
    display: -webkit-inline-box;
    position: relative;
    margin-left: auto;
    margin-right: auto;
}
.bloque-Mayorista{
    display: grid;
}
#mayorista .input-group.bootstrap-touchspin{
    width: 5%!important;
}
/* Mayorista view*/
/* Tooltip container */
.tooltip-container {
    position: relative;
    display: block;
    top: -40px;
    left: -70px;
}

/* Tooltip text */
.tooltip-container .tooltip-text {
    width: 120px;
    background-color: #555;
    color: #fff;
    text-align: center;
    padding: 5px 0;
    border-radius: 6px;

    position: absolute;
    z-index: 1;
    left: 85px;
    bottom: 75px;
    font-size: 11px;

    /* Fade in tooltip */
    opacity: 1;
    transition: opacity 0.3s;
}

/* Tooltip arrow */
.tooltip-container .tooltip-text::after {
    content: " ";
    position: absolute;
    top: 100%; /* At the bottom of the tooltip */
    left: 50%;
    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: #555 transparent transparent transparent;
}

.qty_.low{
    border-bottom-color: var(--staticRed);
    border-bottom-style: solid;
    border-bottom-width: 3px;
}
.qty_.low::after{
    content: " ";
    position: absolute;
    top: 100%;
    left: 50%;
    margin-left: -10px;
    border-width: 10px;
    border-style: solid;
    border-color: var(--staticRed) transparent transparent transparent;
}
.qty_.medium{
    border-bottom-color: var(--hoverOrange);
    border-bottom-style: solid;
    border-bottom-width: 3px;
}
.qty_.medium::after{
    content: " ";
    position: absolute;
    top: 100%;
    left: 50%;
    margin-left: -10px;
    border-width: 10px;
    border-style: solid;
    border-color: var(--hoverOrange) transparent transparent transparent;
}
.qty_.hight{
    border-bottom-color:var(--staticGreen);
    border-bottom-style: solid;
    border-bottom-width: 3px;
}
.qty_.hight::after{
    content: " ";
    position: absolute;
    top: 100%;
    left: 50%;
    margin-left: -10px;
    border-width: 10px;
    border-style: solid;
    border-color: var(--staticGreen) transparent transparent transparent;
}

.formated-price{
   /* top:30px;*/ /* Not necessary in warehouse*/
    font-size:13px;
    position: absolute;
}

.blackbox{
    background: #666;
    margin: 0;
    padding-left: 3px;
    padding-right: 3px;
    color: white;
    width: 60%;
    margin-bottom: 8px;
}
.icon-Mayorista{
    font: normal normal normal 23px/1 FontAwesome;
    line-height: 29px;
    color:#ccc;
}
.selected .icon-Mayorista{
    color:#000!important;
}
.icon-Mayorista:hover{
    color:#000!important;
    transition: all 0.3s ease 0s;
}
.sym-mult{
    font-size: 16px;
    padding-left: 5px;
}

/* Botonchitos, es necesario para themes del S.O  que cambien inputs */
/*
.input-group .input-group-btn > .btn{
    background-color:
}
*/

@media only screen and (max-width: 991px){
    .formated-price{
        top:90%;
        left: 27%;
    }
    [itemprop="name"]{
        display:grid;
    }
    .blackbox, [itemprop="name"] .title{
        margin-right: auto;
        margin-left: auto;
    }
}
@media only screen and (max-width: 768px){
    .formated-price{
        left: 20%;
    }
}

@media only screen and (max-width: 543px){
    .formated-price{
        left: 10%;
    }
}
.products-list article .row > div{
    height:125px!important;
}
</style>